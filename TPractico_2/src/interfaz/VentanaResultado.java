package interfaz;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.SwingConstants;

public class VentanaResultado extends JDialog {

	private static final long serialVersionUID = 1L;

	private JTextField costoTotal;

	public VentanaResultado(JFrame frmAgmAplication, boolean modal)
	{
		
		super(frmAgmAplication,modal);
		setUndecorated(true);
		setTitle("Resultado AGM");
		setResizable(false);
		setBounds(650, 400, 350, 300);
		getContentPane().setLayout(null);
		
		mostrarResultado();
		botonOk();
	}

	public void mostrarResultado() 
	{
		costoTotal = new JTextField();
		costoTotal.setBackground(Color.BLACK);
		costoTotal.setHorizontalAlignment(SwingConstants.CENTER);
		costoTotal.setForeground(new Color(51, 204, 51));
		costoTotal.setEnabled(false);
		costoTotal.setEditable(false);
		costoTotal.setBorder(null);
		costoTotal.setOpaque(false);
		getContentPane().add(costoTotal);
		costoTotal.setFont(new Font("Dialog", Font.BOLD, 14));
		costoTotal.setColumns(10);
		costoTotal.setBounds(89, 180, 173, 27);
	}
	
	public void setCosto(double c)
	{
		DecimalFormat df = new DecimalFormat("#.0000");
		costoTotal.setText("$ "+String.valueOf(df.format(c)));
	}
	
	private void botonOk() 
	{
		JButton btnOk = new JButton("Ok");
		getContentPane().add(btnOk);
		btnOk.setBounds(140, 244, 70, 23);
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(VentanaResultado.class.getResource("/interfaz/imagenes/ventanaResultado.png")));
		label.setBounds(0, 0, 350, 300);
		getContentPane().add(label);
		
		btnOk.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose();
			}
		});
	}
}
