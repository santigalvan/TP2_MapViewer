package interfaz;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import javax.swing.ImageIcon;

public class VentanaCostos extends JDialog {

	private static final long serialVersionUID = 1L;

	private JTextField cableado;
	private JTextField impuesto;
	private JTextField kilometros;

	public VentanaCostos(JFrame frmAgmAplication, boolean modal)
	{
		super(frmAgmAplication,modal);
		setUndecorated(true);
		setTitle("Configuracion Costos");
		setResizable(false);
		setBounds(650, 400, 350, 300);
		getContentPane().setLayout(null);
		
		campoCableado();
		campoImpuesto();
		campoKm();
		carteles();
		botonOk();
		
		JLabel fondo = new JLabel();
		getContentPane().add(fondo);
		fondo.setIcon(new ImageIcon(VentanaCostos.class.getResource("/interfaz/imagenes/costosMenu.png")));
		fondo.setBounds(0, -11, 344, 300);
	}

	private void campoCableado() 
	{
		cableado = new JTextField();
		cableado.setFont(new Font("Dialog", Font.BOLD, 11));
		cableado.setColumns(10);
		cableado.setBounds(119, 117, 196, 19);
		cableado.setBorder(null);
		cableado.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				esNumero(e, cableado);
			}
		});
		getContentPane().add(cableado);
	}
	private void campoImpuesto() 
	{
		impuesto = new JTextField();
		impuesto.setBorder(null);
		getContentPane().add(impuesto);
		impuesto.setFont(new Font("Dialog", Font.BOLD, 11));
		impuesto.setColumns(10);
		impuesto.setBounds(119, 173, 196, 19);
		impuesto.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				esNumero(e, impuesto);
			}
		});
	}

	private void campoKm() 
	{
		kilometros = new JTextField();
		getContentPane().add(kilometros);
		kilometros.setBorder(null);
		kilometros.setFont(new Font("Dialog", Font.BOLD, 11));
		kilometros.setBounds(119, 222, 196, 19);
		kilometros.setColumns(10);
		kilometros.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				esNumero(e, kilometros);
			}
		});
	}

	private void carteles() 
	{
		JLabel textoCostoPorKM = new JLabel("Cableado por Km:");
		getContentPane().add(textoCostoPorKM);
		textoCostoPorKM.setForeground(Color.WHITE);
		textoCostoPorKM.setBounds(111, 95, 204, 15);
		textoCostoPorKM.setFont(new Font("Dialog", Font.BOLD, 14));
		
		JLabel textoImpuestoProvincial = new JLabel("Impuesto provincial:");
		getContentPane().add(textoImpuestoProvincial);
		textoImpuestoProvincial.setForeground(Color.WHITE);
		textoImpuestoProvincial.setFont(new Font("Dialog", Font.BOLD, 14));
		textoImpuestoProvincial.setBounds(111, 150, 204, 15);
		
		JLabel textoExcesoKM = new JLabel("Impuesto por superar 200 Km:");
		getContentPane().add(textoExcesoKM);
		textoExcesoKM.setForeground(Color.WHITE);
		textoExcesoKM.setFont(new Font("Dialog", Font.BOLD, 14));
		textoExcesoKM.setBounds(111, 202, 223, 15);
	}

	private void botonOk() 
	{
		JButton btnOk = new JButton("Ok");
		getContentPane().add(btnOk);
		btnOk.setBounds(22, 222, 70, 23);

		btnOk.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(camposCompletos())
					dispose();
				else
					JOptionPane.showMessageDialog(null, "Por favor, complete todos los campos!");
			}
		});
	}

	private void esNumero(KeyEvent e, JTextField campo) 
	{
		char caracter = e.getKeyChar(); 
		if (((caracter < '0') || (caracter > '9')) 
		        && (caracter != KeyEvent.VK_BACK_SPACE)
		        && (caracter != '.' || campo.getText().contains(".")) ) 
		{
		            e.consume();
		            JOptionPane.showMessageDialog(null, "Se admiten numeros y un solo punto (.) ");
		}
     } 
		
	private boolean camposCompletos()
	{
		return campoOcupado(cableado) && campoOcupado(impuesto) && campoOcupado(kilometros);
	}
	
	
	private boolean campoOcupado(JTextField campo)
	{
		return campo.getText().length() > 0 ;
	}
	
	
	public double getCableado()
	{
		return Double.parseDouble(cableado.getText());
	}
	
	
	public double getImpuesto()
	{
		return Double.parseDouble(impuesto.getText());
	}
	
	
	public double getKilometros()
	{
		return Double.parseDouble(kilometros.getText());
	}
	
	
	public void setearCampos(double [] costos)
	{
		cableado.setText(String.valueOf(costos[0]));
		impuesto.setText(String.valueOf(costos[1]));
		kilometros.setText(String.valueOf(costos[2]));
	}
}
