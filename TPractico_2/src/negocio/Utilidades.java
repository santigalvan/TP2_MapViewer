package negocio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilidades
{
	// Determina si las aristas estan y si est�n, tienen que respetar el mismo orden
	public static void arraysIguales(ArrayList<Conexion> esperado, ArrayList<Conexion> obtenido)
	{
		assertEquals(esperado.size(), obtenido.size());
		
		for(Conexion conexion: esperado) 
			assertTrue(obtenido.contains(conexion));

		for (int i = 0; i < esperado.size(); i++) 
			assertTrue(obtenido.get(i).equals(esperado.get(i)));
	}
	
	// Caso especial donde las distancias son iguales
	public static void distanciasIguales(ArrayList<Conexion> esperado, ArrayList<Conexion> obtenido)
	{
		assertEquals(esperado.size(), obtenido.size());
		
		for(Conexion conexion: esperado) 
			assertTrue(obtenido.contains(conexion));
	}
	
	public static ArrayList<Conexion> crearAristas(ArrayList<Localidad>localidades, String[] source){
		ArrayList<Conexion> ret = new ArrayList<>();
		
		for(String s: source) {
			ret.add(aristaRegEx(localidades, s));
		}
		
		return ret;
	}
	
	public static  Conexion aristaRegEx(ArrayList<Localidad> localidades, String source) 
	{	
		Pattern pattern = Pattern.compile("\\w(\\.?(\\s?\\w+))*");//Nombres con puntos y espacios intermedios
		ArrayList<String> mid = new ArrayList<String>();
		
		Matcher matcher = pattern.matcher(source);
		while(matcher.find()) {
			mid.add(matcher.group());
		}		
		return new Conexion(obtenerObjeto(localidades, mid.get(0)), obtenerObjeto(localidades, mid.get(1)), new Costos(1,0,0)); //Costos por defecto, solo se evaluar� en base a los pesos
	}
	
	public static Localidad obtenerObjeto(ArrayList<Localidad>localidades, String nombre) 
	{
		for(Localidad l: localidades) {
			if(l.getNombre().equals(nombre))
				return l;
		}
		return null;
	}
	
	public static ArrayList<Localidad> crearLocalidades(String[] source)
	{
		ArrayList<Localidad> ret = new ArrayList<>();
		
		for(String s: source) 
			ret.add(localidadRegEx(s));
		
		return ret;
	}
	
	public static Localidad localidadRegEx(String source) {

		Pattern pattern = Pattern.compile("\\-?\\w(\\.?(\\s?\\w+))*");//Nombres con puntos intermedios y espacios & pesos
		
		ArrayList<String> mid = new ArrayList<>();
		
		Matcher matcherLocalidades = pattern.matcher(source);
		
		while(matcherLocalidades.find()) {
			mid.add(matcherLocalidades.group());
		}
		return new Localidad(mid.get(0), mid.get(1), Integer.valueOf(mid.get(2)), Double.valueOf(mid.get(3)), Double.valueOf(mid.get(4)));
	}
}
