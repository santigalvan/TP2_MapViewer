
package negocio;

import java.util.ArrayList;
import org.junit.Test;

public class AGMTest 
{
	@Test
	public void happyPathTest() 
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] {
				"San Miguel, Buenos Aires, 6764, -34.541913994732255, -58.717288970947266", 
				"Bella Vista, Buenos Aires, 23553, -34.56863420401555, -58.68913650512695", 
				"Munro, Buenos Aires, 2343, -34.5292577800633, -58.52210998535156", 
				"Palermo, Buenos Aires, 2343, -34.580401097084966, -58.42456340789795"});
	
		ArrayList<Conexion> esperado =  AuxiliaresTest.crearAristas(localidades, new String[] {
				"San Miguel     -> Bella Vista", 
				"Munro     		-> Palermo",    
				"Bella Vista    -> Munro"	});	
		
		AGM agm = new AGM();
		ArrayList<Conexion> obtenido = agm.kruskal(localidades, Conexion.conexionesPosibles(localidades, new Costos()));
		
		AuxiliaresTest.arraysIguales(esperado, obtenido);
	}
	
	@Test
	public void sinConexionesTest() 
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] {
				"San Miguel, Buenos Aires, 6764, -34.541913994732255, -58.717288970947266", 
				"Bella Vista, Buenos Aires, 23553, -34.56863420401555, -58.68913650512695", 
				"Munro, Buenos Aires, 2343, -34.5292577800633, -58.52210998535156", 
				"Palermo, Buenos Aires, 2343, -34.580401097084966, -58.42456340789795"});
		AGM agm = new AGM();
		
		ArrayList<Conexion> obtenido = agm.kruskal(localidades, new ArrayList<Conexion>());
		
		AuxiliaresTest.arraysIguales(new ArrayList<Conexion>() , obtenido);
	}
	
	@Test
	public void sinLocalidadesTest()
	{
		AGM agm = new AGM();
		ArrayList<Localidad> localidades = new ArrayList<Localidad>();
		
		ArrayList<Conexion> obtenido = agm.kruskal(localidades, Conexion.conexionesPosibles(localidades, new Costos()));
		
		AuxiliaresTest.arraysIguales(new ArrayList<Conexion>() , obtenido);
	}
	
	@Test
	public void conexionesLejanasTest()
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] {
				"Barcelona, Catalunya, 6759, 41.44272637767212, 2.164306640625", 
				"Chicago, Illinois, 29894878, 38.8225909761771, -89.12109375", 
				"Cali, Valle del Cauca, 2343, 4.302591077119676, -78.75", 
				"Kingston, Jamaica, 2343, 8.581021215641853, -75.146484375",
				"Moscu, Rusia, 2343, -21.28937435586042, 44.296875"});
		ArrayList<Conexion> esperado =  AuxiliaresTest.crearAristas(localidades, new String[] {
				"Cali      -> Kingston", 
				"Kingston  -> Chicago",	 
				"Chicago   -> Barcelona",		
				"Barcelona -> Moscu" });    
		AGM agm = new AGM();
		
		ArrayList<Conexion> obtenido = agm.kruskal(localidades, Conexion.conexionesPosibles(localidades, new Costos()));
		
		AuxiliaresTest.arraysIguales(esperado, obtenido);
	}
	
}