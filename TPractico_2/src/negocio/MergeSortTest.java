package negocio;

import java.util.ArrayList;
import org.junit.Test;

public class MergeSortTest 
{
	@Test(expected = IllegalArgumentException.class)
	public void arrayAristaNullTest()
	{
		ArrayList<Conexion> aristas = null;
		resolver(aristas);
	}
	
	@Test
	public void arrayVacioTest() 
	{
		ArrayList<Conexion> conexiones = new ArrayList<>();
		ArrayList<Conexion> ordenado = new ArrayList<>();
		
		resolver(conexiones);
		AuxiliaresTest.arraysIguales(ordenado, conexiones);
	}
	
	//Coordenadas basadas en datos reales, las sacamos del JMapViewer
	//crearLocalidades construye de la siguiente manera:
	//"(Nombre localidad, Nombre Provincia, Cantidad de habitantes, Latitud, Longitud)"
	@Test
	public void happyPathTest() 
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Fonavi, Buenos Aires, 346, -34.44560067000515, -58.73711585998535", 
				"Tortuguitas, Buenos Aires, 236, -34.47035281133538, -58.7584662437439", 
				"Grand Bourg, Buenos Aires, 343, -34.488819248855194, -58.726537227630615" });
		
		ArrayList<Conexion> instancia = AuxiliaresTest.crearAristas(localidades, new String[] {
				"San Miguel  -> Fonavi",
				"Fonavi      -> Grand Bourg",
				"Tortuguitas -> Fonavi",
				"Grand Bourg ->	Tortuguitas" }); 
		
		ArrayList<Conexion> esperado = AuxiliaresTest.crearAristas(localidades, new String[] {
				"Tortuguitas -> Fonavi",
				"Grand Bourg -> Tortuguitas",
				"Fonavi      -> Grand Bourg",
				"San Miguel  -> Fonavi" }); 
		
		resolver(instancia);
		AuxiliaresTest.arraysIguales(esperado, instancia);
	}

	@Test
	public void unaAristaTest() 
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266", 
				"Moreno, Buenos Aires, 424, -34.6399873560295, -58.78981590270996"});
		
		ArrayList<Conexion> instancia = AuxiliaresTest.crearAristas(localidades, new String[] {
				"San Miguel -> Moreno" });
		
		ArrayList<Conexion> esperado = AuxiliaresTest.crearAristas(localidades, new String[] {
				"San Miguel -> Moreno" });

		resolver(instancia);
		AuxiliaresTest.arraysIguales(esperado, instancia);
	}
	
	@Test
	public void distanciasIgualesTest() 
	{
		ArrayList<Localidad> localidades = AuxiliaresTest.crearLocalidades(new String[] { 
				"San Miguel, Buenos Aires, 747, -34.541913994732255, -58.717288970947266",
				"Fonavi, Buenos Aires, 346, -34.44560067000515, -58.73711585998535",
				"Grand Bourg, Buenos Aires, 343, -34.488819248855194, -58.726537227630615" });
		
		ArrayList<Conexion> instancia = AuxiliaresTest.crearAristas(localidades, new String[] {
				"San Miguel  -> Fonavi",
				"Grand Bourg ->	San Miguel",
				"Fonavi      -> Grand Bourg" });

		ArrayList<Conexion> esperado = AuxiliaresTest.crearAristas(localidades, new String[] {
				"Fonavi      -> Grand Bourg",
				"Grand Bourg ->	San Miguel",
				"San Miguel  -> Fonavi" });	
		
		resolver(instancia);
		AuxiliaresTest.distanciasIguales(esperado, instancia);
	}
	
	public void resolver(ArrayList<Conexion> conexiones) 
	{
		MergeSort<Conexion> merge = new MergeSort<Conexion>(conexiones);
		merge.mergesort();
	}
}
