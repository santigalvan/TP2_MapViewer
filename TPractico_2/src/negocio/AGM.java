package negocio;

import java.util.ArrayList;
//Arbol generador minimo aplicando Kruskal con estructura de datos conjuntos-dijuntos.
public class AGM 
{
	public ArrayList<Conexion> kruskal(ArrayList<Localidad> V, ArrayList<Conexion> E){
		if(E.size() == 0) 
			return new ArrayList<Conexion>();
		return kruskalAlgorithm(V, E);
	}
	
	private ArrayList<Conexion> kruskalAlgorithm(ArrayList<Localidad> localidades, ArrayList<Conexion> conexiones) 
	{
		ArrayList<Conexion> ret = new ArrayList<>();
		MergeSort<Conexion> ordenamiento = new MergeSort<Conexion>(conexiones);
		UnionFind componenteConexa = new UnionFind(localidades);
		Integer aristaDeMenorValor = 0;
		
		ordenamiento.mergesort();
		while(ret.size() < localidades.size()-1)
		{
			Conexion actual = conexiones.get(aristaDeMenorValor);
		
			if(perteneceComponenteConexa(componenteConexa, actual)) 
				ret.add(actual);

			aristaDeMenorValor++;
		}
		return ret;
	}
	
	private boolean perteneceComponenteConexa(UnionFind componenteConexa, Conexion actual) 
	{
		return componenteConexa.union(actual.getDestino(), actual.getOrigen());
	}

} 