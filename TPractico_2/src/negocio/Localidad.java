package negocio;

import java.io.Serializable;
import java.util.Objects;

public class Localidad implements Serializable
{
	private String nombre;
	private double latitud;
	private double longitud;
	
	private String provincia;
	private int habitantes;
	
	private static final long serialVersionUID = 1L;
	
	public Localidad(String nombre, String provincia, int habitantes, double lat, double lon) 
	{
		this.nombre = nombre;
		this.latitud = lat;
		this.longitud = lon;
		this.provincia = provincia;
		this.habitantes = habitantes;
	}
	
	public String getNombre() 
	{
		return nombre;
	}
	
	public double getLatitud() 
	{
		return latitud;
	}
	
	public double getLongitud() 
	{
		return longitud;
	}
	
	public String getProvincia() 
	{
		return provincia;
	}
	
	public int getHabitantes() 
	{
		return habitantes;
	}
	
	@Override
	public String toString() 
	{
		return nombre+latitud+longitud;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null || obj.getClass() != this.getClass()) return false;
		if(this == obj) return true;
		Localidad otra = (Localidad) obj;
		if(this.nombre.equals(otra.nombre) && this.latitud == otra.latitud &&
				this.longitud == otra.longitud && this.habitantes == otra.habitantes &&
				this.provincia == otra.provincia) return true;
		return false;
	}
	
	@Override 
	public int hashCode(){
		return Objects.hash(nombre, latitud, longitud, habitantes, provincia);
	}
	
}
